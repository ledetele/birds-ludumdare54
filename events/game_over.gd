extends Node2D

@onready var words = $Words

var array = ["Today, the little bird is also singing.",
"The little bird has finally gained freedom.",
"You said you would miss the little bird.",
"The living always live, and the dead always die.",
"But you still refuse to give up on saving the little bird."
]

# Called when the node enters the scene tree for the first time.
func _ready():
	var round = Events.round
	words.text = array[Events.round]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	get_tree().change_scene_to_file("res://home.tscn")
	Events.day = 1
	Events.hygiene = 5
	Events.mood = 5
	Events.relationship = 5
	Events.funding = 5
	Events.round += 1

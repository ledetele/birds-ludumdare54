extends Node2D

@onready var shadow = $Shadow
@onready var shadow_3 = $Shadow3
@onready var shadow_4 = $Shadow4

@onready var die_timer = $DieTimer
@onready var _0_timer = $"50Timer"

@onready var bird = $Bird
@onready var bird_2 = $Bird2
@onready var bird_3 = $Bird3
@onready var bird_5 = $Bird5
@onready var bird_6 = $Bird6
@onready var bird_7 = $Bird7
@onready var bird_8 = $Bird8
@onready var bird_9 = $Bird9
@onready var bird_10 = $Bird10
@onready var bird_11 = $Bird11
@onready var bird_12 = $Bird12
@onready var bird_13 = $Bird13
@onready var bird_14 = $Bird14
@onready var bird_15 = $Bird15
@onready var bird_16 = $Bird16

@onready var upfx = $up
@onready var birdfx = $bird
@onready var acceptfx = $accept
@onready var background_music = $BackgroundMusic
@onready var diefx = $die

@onready var hygienebar = $Hygiene
@onready var moodbar = $Mood
@onready var relationshipbar = $Relationship
@onready var fundingbar = $Funding
@onready var daylabel = $Day

signal valuechange()
signal nextday()

# Called when the node enters the scene tree for the first time.
func _ready():
	valuechange.connect(update_value)
	nextday.connect(update_days)
	var resource = load("res://dialogue/1.dialogue")
	#var dialogue_line = await DialogueManager.get_next_dialogue_line(resource, "first")
	DialogueManager.show_example_dialogue_balloon(resource, "first")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_down"):
		upfx.play()
	elif Input.is_action_just_pressed("ui_accept"):
		acceptfx.play()

func update_value():
	if Events.hygiene > 5:
		Events.hygiene = 5
	if Events.mood > 5:
		Events.mood = 5
	if Events.relationship > 5:
		Events.relationship = 5
	if Events.funding > 5:
		Events.funding = 5
	
	hygienebar.value = Events.hygiene
	moodbar.value = Events.mood
	relationshipbar.value = Events.relationship
	fundingbar.value = Events.funding
	update_days()
	
	if Events.day == 10:
		bird_3.show()
		shadow_4.show()
		
	elif Events.day == 20:
		bird_2.show()
		shadow_3.show()
	elif Events.day == 25:
		bird.show()
		shadow.show()
	elif Events.day == 30:
		bird_5.show()
	elif Events.day == 35:
		bird_6.show()
	elif Events.day == 50:
		bird_7.show()
		_0_timer.start()
	elif Events.day == 55:
		bird_8.show()
	elif Events.day == 60:
		bird_9.show()
	elif Events.day == 65:
		bird_10.show()
	elif Events.day == 70:
		bird_11.show()
	elif Events.day == 75:
		bird_12.show()
	elif Events.day == 80:
		bird_13.show()
	elif Events.day == 85:
		bird_14.show()
	elif Events.day == 90:
		bird_14.show()
	elif Events.day == 95:
		bird_15.show()
	elif Events.day == 100:
		bird_16.show()	
	
	if Events.hygiene <= 0 or Events.mood <= 0 or Events.relationship <= 0 or Events.funding <= 0:
		diefx.play()
		die_timer.start()
		
	
func update_days():
	Events.day += 1
	daylabel.text = "DAY " + str(Events.day)
	birdfx.play()


func _on_timer_timeout():
	background_music.play()

func _on_50timer_timeout():
	Events.hygiene -= 1
	Events.mood -= 1
	Events.relationship -=1
	Events.funding -=1
	update_value()

func _on_die_timer_timeout():
	get_tree().change_scene_to_file("res://events/game_over.tscn")
